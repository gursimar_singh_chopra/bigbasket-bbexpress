import json, sys , gzip

fo = gzip.open(sys.argv[1])
fw = open(sys.argv[1]+'_unique', 'w')

urlhs = set()
total = 0
for line in fo:
    try:
        line = json.loads(line)
        line['uid'] = line['pincode_s']+line['urlh']
    except:
            continue
    try:    
        if line.get('uid'):
            total += 1
            if line['uid'] not in urlhs:
                urlhs.add(line['uid'])
                fw.write(json.dumps(line)+'\n')
    except:
            continue



print str(len(urlhs)) + ' unique out of ' + str(total)

